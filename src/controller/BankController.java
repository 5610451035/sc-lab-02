package controller;




		/*เลือกตัวแปร   class BankAccount และ  class InvestmentFrame เป็น   attribute ของ  controller 
		 เพราะ เป็นตัวแปรที่ใช้ภายในคลาส  controller เพื่อเป็นการเรียก model และ   gui มาประสานงานกัน */
		 




import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import view.InvestmentFrame;
import model.BankAccount;

public class BankController {
		
	   private BankAccount account;
	   private static final double INITIAL_BALANCE = 1000;  
	   private static final int FRAME_WIDTH = 550;
	   private static final int FRAME_HEIGHT = 100;
	   
	   ActionListener list;
		InvestmentFrame frame;
	  
	   
	   public static void main(String[] args){
		   new BankController();
	   }
	   
	   public BankController()
	   {  
		  list = new AddInterestListener();
	      account = new BankAccount(INITIAL_BALANCE);

	      // Use instance variables for components 
	     

	      // Use helper methods 
	      frame = new InvestmentFrame();
	      frame.createTextField();
	      frame.createButton(list);
	      frame.createPanel();
	      
	      frame.setVisible(true);
	      frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	   }
    	         
		
		
	   class AddInterestListener implements ActionListener
	     {
	        public void actionPerformed(ActionEvent event)
	        {
	           double rate = Double.parseDouble(frame.getRateField());
	           double interest = account.getBalance() * rate / 100;
	           account.deposit(interest);
	           frame.setResult(account.getBalance()+"");
	        }            
	     }
	   
	   
}
	      
