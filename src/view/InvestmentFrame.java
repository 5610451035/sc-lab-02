package view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.BankAccount;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    

   private static final double DEFAULT_RATE = 0; 
   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JPanel panel;
   private JLabel resultLabel;
   private BankAccount account;
   private JTextArea showResults;
   
   

   public void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   public void createButton(ActionListener listener)
   {
      button = new JButton("Add Interest");
      button.addActionListener(listener); 
      resultLabel = new JLabel();

      
   }


   public void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }

   
   public String getRateField() 
   {
	   return rateField.getText();
   }

   
   public void setResult(String string) 
   {
	   resultLabel.setText(string);
	
	
   }


}
